![](Logo_dTeilorg_Completo.png) 

# Indice

- Web: https://detailorg.com
- App: https://dpoint.detailorg.com
- Grupo de Gitlab: https://gitlab.com/detailorg
  - Guía de Gitlab: https://gitlab.com/detailorg/paso-a-paso-en-gitlab
  - Guía de Markdown: https://gitlab.com/detailorg/estilo-de-publicacion-con-markdown
- MockUp en Figma: https://www.figma.com/file/VcE6vDRk81HPBXDD8WwuWE/dPoint?node-id=864%3A3535
- Grupo de Telegram: https://t.me/joinchat/HFc6gN0LkbRJB0XJ
